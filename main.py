from flask import Flask,request,jsonify
from werkzeug.security import generate_password_hash,check_password_hash
from model.radnik_entity import Radnik
from model.korisnik_entity import Korisnik
import datetime
import jwt

from service.grad_service import grad_ns
from service.deo_grada_service import deo_grada_ns
from service.korisnik_service import korisnik_ns
from service.firma_service import firma_ns
from service.deo_firme_service import deo_firme_ns
from service.sto_service import sto_ns
from service.radnik_service import radnik_ns
from service.privilegija_service import privilegija_ns
from service.porudzbina_service import porudzbina_ns
from service.status_service import status_ns
from service.istorija_porudzbine_service import istorija_porudzbine_ns
from service.stavka_service import stavka_ns
from service.podkategorija_service import podkategorija_ns
from service.kategorija_service import kategorija_ns
from service.fajl_service import fajl_ns

from service.abstract_service import AbstractService
from service.api import apiv1

key = 'restaurantapp'

app = Flask(__name__, static_url_path="", static_folder='www', template_folder='templates')
app.config["SECRET_KEY"] = key
app.register_blueprint(apiv1)


@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html")



if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True, debug=True)


