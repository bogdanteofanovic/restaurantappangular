from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, String, ForeignKey, Integer
from sqlalchemy.orm import relationship, backref


class DeoGrada(AbstractEntity):

    __tablename__ = 'deograda'

    naziv = Column(String, name='naziv')
    grad_id = Column(Integer, ForeignKey('grad.id'), name='GradId')
    grad = relationship("Grad", backref=backref("deoGrada"), lazy="joined")


    def __init__(self, naziv, grad):

        self.naziv = naziv
        self.grad = grad
