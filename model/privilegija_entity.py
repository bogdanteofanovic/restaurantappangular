from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref


class Privilegija(AbstractEntity):

    __tablename__ = 'privilegija'

    korisnik = Column(Boolean, name='korisnik')
    administrator = Column(Boolean, name='administrator')
    menadzer = Column(Boolean, name='menadzer')

    radnik_id = Column(Integer, ForeignKey('radnik.id'), name='radnikId')



    def __init__(self, radnik, korisnik=True, administrator=False, menadzer=False):

        self.korisnik = korisnik
        self.administrator = administrator
        self.menadzer = menadzer
        self.radnik = radnik
