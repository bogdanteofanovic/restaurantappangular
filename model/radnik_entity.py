from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, Boolean, String,ForeignKey
from sqlalchemy.orm import relationship, backref


class Radnik(AbstractEntity):

    __tablename__ = 'radnik'

    ime = Column(String, name='ime')
    prezime = Column(String, name='prezime')
    email = Column(String, name='email')
    telefon = Column(String, name='telefon')
    korisnicko_ime = Column(String, name='korisnickoIme')
    lozinka = Column(String, name='lozinka')
    aktivan = Column(Boolean, name='aktivan')

    deoFirmeId = Column(Integer, ForeignKey('deoFirme.id'), name='deoFirmeId')
    deoFirme = relationship("DeoFirme", backref=backref("radnik"), lazy="joined")
    privilegije = relationship("Privilegija", cascade="all,delete",backref=backref("radnik"),lazy="joined")

    def __init__(self, ime, prezime, email, telefon, korisnicko_ime, lozinka, deoFirme, aktivan=True):

        self.ime = ime
        self.prezime = prezime
        self.email = email
        self.telefon = telefon
        self.korisnicko_ime = korisnicko_ime
        self.lozinka = lozinka
        self.deoFirme = deoFirme
        self.aktivan = aktivan
