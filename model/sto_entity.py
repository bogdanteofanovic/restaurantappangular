from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, Boolean, String,ForeignKey
from sqlalchemy.orm import relationship, backref


class Sto(AbstractEntity):

    __tablename__ = 'sto'


    naziv = Column(String, name='naziv')
    deoFirmeId = Column(Integer, ForeignKey('deoFirme.id'), name='deoFirmeId')
    deoFirme = relationship("DeoFirme", backref=backref("sto"),lazy="joined")
    aktivan = Column(Boolean, name="aktivan")

    def __init__(self, naziv, deoFirme, aktivan=True):

        self.naziv = naziv
        self.deoFirme = deoFirme
        self.aktivan = aktivan
