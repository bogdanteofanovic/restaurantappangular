from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, String,ForeignKey
from sqlalchemy.orm import relationship, backref


class PodKategorija(AbstractEntity):

    __tablename__ = 'podkategorija'

    naziv = Column(String, name='naziv')
    kategorija_id = Column(Integer, ForeignKey('kategorija.id'), name='kategorijaId')
    kategorija = relationship("Kategorija", backref=backref("podkategorija"),lazy="joined")

    def __init__(self, naziv, kategorija):

        self.naziv = naziv
        self.kategorija = kategorija
