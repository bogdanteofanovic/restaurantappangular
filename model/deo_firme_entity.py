from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, String,Integer, ForeignKey
from sqlalchemy.orm import relationship, backref


class DeoFirme(AbstractEntity):

    __tablename__ = 'deoFirme'

    naziv = Column(String, name='naziv')
    firmaId = Column(Integer, ForeignKey('firma.id'), name='firmaId')
    firma = relationship("Firma", backref=backref("deoFirme"), lazy="joined")

    def __init__(self, naziv, firma):
        self.naziv = naziv
        self.firma = firma