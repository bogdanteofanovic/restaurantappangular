from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, Float, Boolean, DateTime,ForeignKey,String
from sqlalchemy.orm import relationship, backref

class Porudzbina(AbstractEntity):

    __tablename__ = 'porudzbina'

    datum_otvaranja = Column(DateTime, name='DatumOtvaranja')
    datum_zatvaranja = Column(DateTime, name='DatumZatvaranja')
    ukupna_cena = Column(Float, name='UkupnaCena')

    korisnik_id = Column(Integer, ForeignKey('korisnik.id'), name='KorisnikId')
    korisnik = relationship("Korisnik", backref=backref("porudzbina"),lazy="joined")

    radnik_id = Column(Integer, ForeignKey('radnik.id'), name='RadnikId')
    radnik = relationship("Radnik", backref=backref("porudzbina"), lazy="joined")

    sto_id = Column(Integer, ForeignKey('sto.id'), name='StoId')
    sto = relationship("Sto", backref=backref("porudzbina"), lazy="joined")

    status_porudzbine_id = Column(Integer,ForeignKey("statusPorudzbine.id"), name="StatusPorudzbineId")
    status_porudzbine = relationship("StatusPorudzbine", backref=backref("porudzbina"), lazy="joined")

    dostava_ime = Column(String, name="DostavaIme")
    dostava_prezime = Column(String, name="DostavaPrezime")
    dostava_telefon = Column(String, name="DostavaTelefon")
    dostava_ulica = Column(String, name="DostavaUlica")
    dostava_stan = Column(String, name="DostavaStan")
    dostava_sprat = Column(String, name="DostavaSprat")
    dostava_deo_grada_id = Column(Integer, ForeignKey('deograda.id'), name='DostavaDeoGradaId')
    dostava_deo_grada = relationship("DeoGrada", backref=backref("porudzbina"), lazy="joined")
    dostava = Column(Boolean, name="Dostava")
    licno_preuzimanje = Column(Boolean, name="Licno")
    online = Column(Boolean, name="Online")

    napomena = Column(String, name="Napomena")

    istorija_porudzbine = relationship("IstorijaPorudzbine", lazy='subquery')

    def __init__(self, status_porudzbine, datum_otvaranja, napomena, datum_zatvaranja = None, ukupna_cena=0, korisnik=None, radnik=None, sto=None, dostava=False, licno_preuzimanje=False, online=False,
                 dostava_ime = None, dostava_prezime = None, dostava_telefon = None, dostava_ulica = None, dostava_stan = None, dostava_sprat = None, dostava_deo_grada = None):

        self.datum_otvaranja = datum_otvaranja
        self.datum_zatvaranja = datum_zatvaranja
        self.ukupna_cena = ukupna_cena
        self.korisnik = korisnik
        self.radnik = radnik
        self.sto = sto
        self.status_porudzbine = status_porudzbine
        self.dostava = dostava
        self.licno_preuzimanje = licno_preuzimanje
        self.online = online
        self.napomena = napomena
        self.dostava_ime = dostava_ime
        self.dostava_prezime = dostava_prezime
        self.dostava_telefon = dostava_telefon
        self.dostava_ulica = dostava_ulica
        self.dostava_stan = dostava_stan
        self.dostava_sprat = dostava_sprat
        self.dostava_deo_grada = dostava_deo_grada
