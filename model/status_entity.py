from model.abstract_entity import AbstractEntity
from sqlalchemy import Column,String


class StatusPorudzbine(AbstractEntity):

    __tablename__ = 'statusPorudzbine'

    status = Column(String, name='status')

    def __init__(self, status):

        self.status = status
