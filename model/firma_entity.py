from model.abstract_entity import AbstractEntity
from model.grad_entity import Grad
from sqlalchemy import Column, Integer, String,ForeignKey
from sqlalchemy.orm import relationship, backref


class Firma(AbstractEntity):

    __tablename__ = 'firma'

    naziv = Column(String, name='naziv')
    kontakt_telefon = Column(String, name='kontaktTelefon')
    kontakt_email = Column(String, name='kontaktEmail')
    web_adresa = Column(String, name='webAdresa')
    ulica = Column(String, name='ulica')
    broj = Column(String, name='broj')
    gradId = Column(Integer, ForeignKey('grad.id'), name='gradId')
    grad = relationship("Grad", backref=backref("firma"),lazy="joined")

    def __init__(self, naziv, kontakt_telefon, kontakt_email, web_adresa, ulica, broj, grad):

        self.naziv = naziv
        self.kontakt_telefon = kontakt_telefon
        self.kontakt_email = kontakt_email
        self.web_adresa = web_adresa
        self.ulica = ulica
        self.broj = broj
        self.grad = grad