from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, Float, String,ForeignKey, Boolean
from sqlalchemy.orm import relationship, backref


class Stavka(AbstractEntity):

    __tablename__ = 'stavka'

    naziv = Column(String, name='naziv')
    opis = Column(String, name='opis')
    tezina = Column(Float, name='tezina')
    cena = Column(Float, name='cena')
    podkategorija_id = Column(Integer, ForeignKey('podkategorija.id'), name='PodkategorijaId')
    podkategorija = relationship("PodKategorija", backref=backref("stavka"),lazy="joined")
    aktivna = Column(Boolean, name='aktivna')
    fajlovi = relationship("Fajl", cascade="all,delete", backref=backref("stavka"), lazy="joined")

    def __init__(self, naziv, opis, tezina, cena, podkategorija, aktivna = True):

        self.naziv = naziv
        self.opis = opis
        self.tezina = tezina
        self.cena = cena
        self.podkategorija = podkategorija
        self.aktivna = aktivna
