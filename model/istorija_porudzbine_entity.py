from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, DateTime, Float, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from datetime import datetime


class IstorijaPorudzbine(AbstractEntity):

    __tablename__ = 'istorijaporudzbine'

    datum = Column(DateTime, name='datum')

    stavka_id = Column(Integer, ForeignKey('stavka.id'), name='StavkaId')
    stavka = relationship("Stavka", backref=backref("istorijaporudzbine"), lazy="joined")

    porudzbina_id = Column(Integer, ForeignKey('porudzbina.id'), name='PorudzbinaId')
    porudzbina = relationship("Porudzbina", backref=backref("istorijaporudzbine"),lazy="joined")

    radnik_id = Column(Integer, ForeignKey('radnik.id'), name='RadnikId')
    radnik = relationship("Radnik", backref=backref("istorijaporudzbine"),lazy="joined")

    korisnik_id = Column(Integer, ForeignKey('korisnik.id'), name='KorisnikId')
    korisnik = relationship("Korisnik", backref=backref("istorijaporudzbine"),lazy="joined")

    cena = Column(Float, name='Cena')
    obrisana = Column(Boolean, name='Obrisana')

    def __init__(self, stavka, porudzbina, radnik, korisnik, datum ,obrisana = False):

        self.datum = datum
        self.stavka = stavka
        self.porudzbina = porudzbina
        self.radnik = radnik
        self.korisnik = korisnik
        self.cena = self.stavka.cena
        self.obrisana = obrisana
