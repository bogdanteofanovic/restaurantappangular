from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, String, Boolean


class Kategorija(AbstractEntity):

    __tablename__ = 'kategorija'

    naziv = Column(String, name='naziv')
    aktivna = Column(Boolean, name='aktivna')

    def __init__(self, naziv, aktivna=True):

        self.naziv = naziv
        self.aktivna = aktivna
