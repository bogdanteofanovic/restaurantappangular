from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, String,ForeignKey, TEXT
from sqlalchemy.orm import relationship, backref


class Fajl(AbstractEntity):

    __tablename__ = 'fajl'

    fajl = Column(TEXT, name='fajl')
    stavka_id = Column(Integer, ForeignKey('stavka.id'), name='stavkaId')


    def __init__(self, fajl, stavka):

        self.fajl = fajl
        self.stavka = stavka
