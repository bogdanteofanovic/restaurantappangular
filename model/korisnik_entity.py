from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, Integer, Boolean, String,ForeignKey,Text
from sqlalchemy.orm import relationship, backref


class Korisnik(AbstractEntity):
    
    __tablename__ = 'korisnik'

    ime = Column(String, name='ime')
    prezime = Column(String, name='prezime')
    telefon = Column(String, name='telefon')
    email = Column(String, name='email')
    korisnicko_ime = Column(String, name='korisnickoIme')
    lozinka = Column(Text, name='lozinka')
    ulica = Column(String, name='ulica')
    stan = Column(String, name='stan')
    sprat = Column(Integer, name='sprat')

    deo_grada_id = Column(Integer, ForeignKey('deograda.id'), name='DeoGradaId')
    deo_grada = relationship("DeoGrada", backref=backref("korisnik"), lazy="joined")

    napomena = Column(String, name='napomena')
    aktivan = Column(Boolean, name='aktivan')

    def __init__(self, ime, prezime, telefon, email, korisnicko_ime, lozinka, ulica, stan, sprat, deo_grada, napomena, aktivan = 1):

        self.ime = ime
        self.prezime = prezime
        self.telefon = telefon
        self.email = email
        self.korisnicko_ime = korisnicko_ime
        self.lozinka = lozinka
        self.ulica = ulica
        self.stan = stan
        self.sprat = sprat
        self.deo_grada = deo_grada
        self.napomena = napomena
        self.aktivan = aktivan


