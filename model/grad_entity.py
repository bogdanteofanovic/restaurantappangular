from model.abstract_entity import AbstractEntity
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship


class Grad(AbstractEntity):

    __tablename__ = 'grad'

    naziv = Column(String, name='naziv')
    postanski_broj = Column(String, name='postanskiBroj')

    delovi_grada = relationship("DeoGrada", lazy='subquery')

    def __init__(self,naziv,postanski_broj):

        self.naziv = naziv
        self.postanski_broj = postanski_broj
