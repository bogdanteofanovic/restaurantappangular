from dictalchemy import DictableModel
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
Base = declarative_base(cls=DictableModel)

class AbstractEntity(Base):

    __abstract__ = True

    id = Column(Integer,name='id', primary_key=True)

    def __str__(self):

        return str(self.id)

    def __repr__(self):

        return str(self.id)



