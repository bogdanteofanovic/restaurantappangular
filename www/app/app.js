(function(angular){
    
    var app = angular.module("app", ["ui.router","ui.bootstrap","naif.base64"]);

    

    app.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider){

        $stateProvider.state("aktivne_porudzbine", {
            url: "/aktivne_porudzbine",
            templateUrl: "app/controler/porudzbina/aktivne_porudzbine.html",
            controller: "PorudzbinaCtrl",
            controllerAs: "pc"
        }).state("sve_porudzbine", {
            url: "/porudzbine",
            templateUrl: "/app/controler/porudzbina/porudzbina.html",
            controller: "PorudzbinaCtrl",
            controllerAs: "pc"
        }).state("adrese", {
            url: "/adrese",
            templateUrl: "/app/controler/adresa/adresa.html",
            controller: "AdresaCtrl",
            controllerAs: "ac"
        }).state("korisnici", {
            url: "/korisnici",
            templateUrl: "/app/controler/korisnik/korisnik.html",
            controller: "KorisnikCtrl",
            controllerAs: "kc"
        }).state("radnici", {
            url: "/radnici",
            templateUrl: "/app/controler/radnik/radnik.html",
            controller: "RadnikCtrl",
            controllerAs: "rc"
        }).state("deloviFirme", {
            url: "/deloviFirme",
            templateUrl: "/app/controler/deoFirme/deoFirme.html",
            controller: "DeoFirmeCtrl",
            controllerAs: "df"
        }).state("firme", {
            url: "/firme",
            templateUrl: "/app/controler/firma/firma.html",
            controller: "FirmaCtrl",
            controllerAs: "fc"
        }).state("kategorije", {
            url: "/kategorije",
            templateUrl: "/app/controler/kategorija/kategorija.html",
            controller: "KategorijaCtrl",
            controllerAs: "kc"
        }).state("podkategorije", {
            url: "/podkategorije",
            templateUrl: "/app/controler/podkategorija/podkategorija.html",
            controller: "PodkategorijaCtrl",
            controllerAs: "pkc"
        }).state("stavke", {
            url: "/stavke",
            templateUrl: "/app/controler/stavka/stavka.html",
            controller: "StavkaCtrl",
            controllerAs: "stvk"
        }).state("stolovi", {
            url: "/stolovi",
            templateUrl: "/app/controler/sto/sto.html",
            controller: "StoCtrl",
            controllerAs: "sc"
        }).state("delovigrada", {
            url: "/delovigrada",
            templateUrl: "/app/controler/deoGrada/deoGrada.html",
            controller: "DeoGradaCtrl",
            controllerAs: "dgc"
        }).state("login", {
            url: "/admin_login",
            templateUrl: "/app/controler/login/login.html",
            controller: "LoginCtrl",
            controllerAs: "lc"
        }).state("istorije_porudzbine", {
            url: "/istorije_porudzbine",
            templateUrl: "/app/controler/istorijaPorudzbine/istorijaPorudzbine.html",
            controller: "IstorijaPorudzbineCtrl",
            controllerAs: "ipc"
        }).state("gradovi", {
            url: "/gradovi",
            templateUrl: "/app/controler/grad/grad.html",
            controller: "GradCtrl",
            controllerAs: "gc"
        });
        
        //ukoliko je navedena neka druga ruta
        $urlRouterProvider.otherwise("/admin_login");
    }])

    

    app.factory('abstractService', ['$http', function($http){
        var abstractService = {};
        var config = {};
        
        var isloggedin = function(){

            if(sessionStorage.getItem('restaurantapp_radnik_data')){
            var token =  JSON.parse(sessionStorage.getItem('restaurantapp_radnik_data'));
            config = {
                headers : {
                "X-API-KEY":token.token
                
            }
            }
        }
        }

        
        
        console.log(sessionStorage.getItem('restaurantapp_radnik_data'));
        //get one entity for id
        abstractService.getOneId = function(entity,id,cb){            
            $http.get("v1/"+entity+"/"+id, config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data);}    
        )};
        //get all entities
        abstractService.getAll = function(entity,cb){
            isloggedin();
            $http.get("v1/"+entity+"/", config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                };}
        )};
        //add one entity
        abstractService.addOne = function(entity,data,cb){
            $http.post("v1/"+entity+"/",data, config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                };}
        )};
        //edit one entity
        abstractService.editOne = function(entity,data,id,cb){
            isloggedin();
            $http.put("v1/"+entity+"/"+id,data,config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                }
                ;}
        )};
        //delete one entity
        abstractService.deleteOne = function(entity,id,cb){
            $http.delete("v1/"+entity+"/"+id, config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                };}
        )};
        //query data
        abstractService.queryData = function(entity,data,cb){
            $http.post("v1/"+entity+"/query/",data, config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                };}
        )};
        abstractService.getQueryData = function(entity,data,cb){
            $http.get("v1/"+entity+"/query/"+data, config).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                };}
        )};
        abstractService.printData = function(entity,data,cb){
            $http.post("v1/"+entity+"/print/", data, { responseType: 'arraybuffer' }).then(
            function(response){cb(response.data,null);},
            function(response){cb(null,response.data)
                if(response.status == 420){
                    sessionStorage.removeItem('restaurantapp_radnik_data');
                    window.location.reload();
                }
                if(response.status == 403){
                    alert(response.data);
                    
                };}
        )};


        return abstractService;
        
    }]);

    
    
   

     
})(angular);