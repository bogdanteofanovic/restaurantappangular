(function (angular){

	var app = angular.module("app");


	app.controller('PorudzbinaCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];

		this.prikaziSliku = false;

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};
		this.preuzimanje = [{'naziv':'Lično', 'id':'licno'},{'naziv':'U restoranu', 'id':'restoran'},{'naziv':'Dostava', 'id':'dostava'}];
		this.stolovi = [];
		this.korisnik = {};
		this.korisnici = [];
		this.radnik = {"id":null};
		this.porudzbine = [];
		this.temp_kategorije = [];
		this.temp_podkategorije = [];
		this.temp_istorija_porudzbine = {};
		this.temp_stavke = [];
		this.temp_stavka = {};
		this.temp_istorije_porudzbine = [];
		this.temp_porudzbina ={};
		this.aktivne_porudzbine = [];
		this.porudzbina = {"dostavljanje":""};
		this.dopuna_istorije = [];
		this.adresa = {};
		this.gradovi = [];
		this.delovi_grada = [];
		this.istorija_za_porudzbinu = {"porudzbina_id":null}
		this.temp_korisnici_query = [];
		this.porudzbina_query = {};
		this.statusi = [];
		this.radnici = [];


		this.blob = null;

		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				temp_korisnik = JSON.parse(temp_korisnik);
				that.preuzimanje = [{'naziv':'Lično', 'id':'licno'},{'naziv':'U restoranu', 'id':'restoran'},{'naziv':'Dostava', 'id':'dostava'}];
				that.stolovi = [];
				that.korisnik = {};
				that.korisnici = [];
				that.radnik = {"id":temp_korisnik.id};
				that.porudzbine = [];
				that.temp_kategorije = [];
				that.temp_podkategorije = [];
				that.temp_istorija_porudzbine = {};
				that.temp_stavke = [];
				that.temp_stavka = {};
				that.temp_istorije_porudzbine = [];
				that.temp_porudzbina ={};
				that.aktivne_porudzbine = [];
				that.porudzbina = {"dostavljanje":""};
				that.dopuna_istorije = [];
				that.adresa = {};
				that.gradovi = [];
				that.delovi_grada = [];
				that.istorija_za_porudzbinu = {"porudzbina_id":null}
				that.temp_korisnici_query = [];
				that.porudzbina_query = {};
				that.statusi = [];
				that.radnici = [];

				this.ucitajStolove();	
				this.ucitajKategorije();
				this.ucitajPorudzbine();
				this.ucitajAktivnePorudzbine();
				this.ucitajGradove();
				this.ucitajDeloveGrada();
				this.ucitajStatuse();
				this.ucitajRadnike();

			}else{
				$state.go('login');
			}

			
		}

		this.pretrazi = function(){
			
			delete that.porudzbina.dostavljanje;

			if(that.porudzbina.datum_otvaranja__from){
				that.porudzbina.datum_otvaranja__from = new Date(that.porudzbina.datum_otvaranja__from);
				that.porudzbina.datum_otvaranja__from.setDate(that.porudzbina.datum_otvaranja__from.getDate()+1);
				that.porudzbina.datum_otvaranja__from = that.porudzbina.datum_otvaranja__from.toISOString().substring(0,10);
			}
			if(that.porudzbina.datum_otvaranja__to){
				that.porudzbina.datum_otvaranja__to = new Date(that.porudzbina.datum_otvaranja__to);
				that.porudzbina.datum_otvaranja__to.setDate(that.porudzbina.datum_otvaranja__to.getDate()+1);
				that.porudzbina.datum_otvaranja__to = that.porudzbina.datum_otvaranja__to.toISOString().substring(0,10);
			}

			if(that.porudzbina.datum_zatvaranja__from){
				that.porudzbina.datum_zatvaranja__from = new Date(that.porudzbina.datum_zatvaranja__from);
				that.porudzbina.datum_zatvaranja__from.setDate(that.porudzbina.datum_zatvaranja__from.getDate()+1);
				that.porudzbina.datum_zatvaranja__from = that.porudzbina.datum_zatvaranja__from.toISOString().substring(0,10);
			}
			if(that.porudzbina.datum_zatvaranja__to){
				that.porudzbina.datum_zatvaranja__to = new Date(that.porudzbina.datum_zatvaranja__to);
				that.porudzbina.datum_zatvaranja__to.setDate(that.porudzbina.datum_zatvaranja__to.getDate()+1);
				that.porudzbina.datum_zatvaranja__to = that.porudzbina.datum_zatvaranja__to.toISOString().substring(0,10);
			}

			console.log(that.porudzbina);
			abstractService.queryData("porudzbina",that.porudzbina,function(resp,err){
				
				console.log(resp);
				that.porudzbine=resp;
				
				that.porudzbina_query = {};
				that.porudzbina = {};
			});
		}

		this.promenaStatusa = function(id,status){
			console.log('promena statusa',status);
			var status_porudzbine = {"status":status}
			abstractService.editOne("porudzbina",status_porudzbine, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Porudzbina izmenjena!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });
		}

		this.ucitajStatuse = function(){
			that.statusi = [];
			abstractService.getAll("status",function(resp,err){
				
				that.statusi=resp;
			});

		};

		this.ucitajRadnike = function(){
			that.radnici = [];
			abstractService.getAll("radnik",function(resp,err){
				
				that.radnici=resp;
			});

		};


		this.ucitajGradove = function(){
			that.gradovi = [];
			abstractService.getAll("grad",function(resp,err){
				
				that.gradovi=resp;
			});

		};

		this.ucitajDeloveGrada = function(){
			that.delovi_grada = [];
			abstractService.getAll("deoGrada",function(resp,err){
				console.log(resp);
				that.delovi_grada=resp;
			});

		};

		this.ucitajDeloveGradaZaId = function(id){
			that.delovi_grada = [];
			var temp_deo_grada = {'grad_id':id}
			abstractService.queryData("deoGrada",temp_deo_grada,function(resp,err){
				console.log(temp_deo_grada);
				console.log(resp);
				that.delovi_grada=resp;
			});

		};

		this.unesiKorisnika = function(){
			
			

			
			abstractService.addOne("korisnik", that.korisnik, function(resp,err){
				if(resp){
					that.porudzbina.podaci_dostave = that.korisnik;
					that.korisnik = {};

					that.addAlert('Korisnik unet!');
					$scope.addKorisnikModal.hide();
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
			
		}

		this.podaciDostave = function(){
			console.log(that.porudzbina.podaci_dostave);
		}			
		this.getKorisnici = function(tel) {
			console.log(tel);
			var temp_telefon = {
				"telefon":null
			};
			temp_telefon.telefon = tel;
			console.log(temp_telefon);
			console.log(that.porudzbina.dostavljanje);
			abstractService.queryData('korisnik', temp_telefon, function(resp){
				that.temp_korisnici_query = resp;
			});
			console.log(that.temp_korisnici_query);
			return that.temp_korisnici_query;
			
			
		};

		this.dostavaFalse = function(){
			console.log('dostavaFalse');
			that.porudzbina.korisnik = null;
			that.porudzbina.podaci_dostave = null;
			that.porudzbina.dostava = false;
			that.porudzbina.dostavljanje = '';

		}




		this.ucitajKategorije = function(){
			abstractService.getAll("kategorija",function(resp,err){
				that.temp_kategorije = resp;
				if(err!=null){that.addAlert("Doslo je do greske: " + err);}
				
			});

		}

		this.ucitajPodkategorijeZaIdKat = function(id){
			var katId = {"kategorija_id":null};
			katId.kategorija_id = id;
			console.log(id);
			that.temp_podkategorije = [];
			abstractService.queryData("podkategorija",katId,function(resp,err){
				that.temp_podkategorije = resp;
				if(err!=null){that.addAlert("Doslo je do greske: " + err);}
				
			});

		}

		this.ucitajStavkeZaId = function(id){
			var katId = {"podkategorija_id":null};
			katId.podkategorija_id = id;
			katId.aktivna = true;
			console.log(id);
			that.temp_stavke = [];
			abstractService.queryData("stavka",katId,function(resp,err){
				that.temp_stavke = resp;
				if(err!=null){that.addAlert("Doslo je do greske: " + err);}
				
			});

		}



		this.dodajTempIstoriju = function(){
			
			that.temp_istorija_porudzbine.stavka = that.temp_stavka.stavka;
			that.temp_istorija_porudzbine.radnik_id = that.radnik.id;
			that.temp_istorija_porudzbine.radnik_id = that.radnik.id;
			that.temp_istorija_porudzbine.id = that.temp_istorije_porudzbine.length + 1;
			
			if(that.porudzbina.id){

				that.dopuna_istorije.push(that.temp_istorija_porudzbine);
				that.temp_istorije_porudzbine.push(that.temp_istorija_porudzbine);
				that.porudzbina.dopuna_istorije = that.dopuna_istorije;
				

			}else{
				
				that.temp_istorije_porudzbine.push(that.temp_istorija_porudzbine);
				
			}

			that.temp_stavka = {};
			that.temp_istorija_porudzbine = {};
		}

		this.ucitajPorudzbine = function(){
			abstractService.getAll("porudzbina",function(resp,err){
				that.porudzbine = resp;
				if(err!=null){that.addAlert("Doslo je do greske: " + err);}
				
			});
		}

		this.ucitajKorisnike = function(){
			abstractService.getAll("korisnik",function(resp,err){
				that.korisnici = resp;
			});
		}

		this.ucitajStolove = function(){
			abstractService.getAll("sto",function(resp,err){
				that.stolovi = resp;
				console.log(that.stolovi);
			});
		}

		this.ucitajAktivnePorudzbine = function(){
			abstractService.getQueryData("porudzbina", "aktivne", function(resp,err){
				that.aktivne_porudzbine = resp;
				for(p of that.aktivne_porudzbine){
					p.suma = 0.0;
					for(s of p.stavke){
						p.suma = p.suma + parseFloat(s.cena);
					}
				}
				if(err!=null){that.addAlert("Doslo je do greske: " + err);}
				
			});

		}

		

		this.stampajPorudzbinu = function(){
			that.porudzbina.istorija = that.temp_istorije_porudzbine;
			datum = new Date();
			that.porudzbina.datum = datum.toJSON();
			abstractService.printData("porudzbina", that.porudzbina, function(resp,err){
				that.blob = new Blob([resp], { type: 'application/pdf' });
				var fileURL = URL.createObjectURL(that.blob);
				console.log("test url");
				$window.open(fileURL);
				
			});
			
		}

		this.obrisiPorudzbinu = function(id){
			abstractService.deleteOne("porudzbina", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Porudžbina obrisana!');
					that.ucitajPorudzbine();
				}
				
			});
		}
		this.ucitajPorudzbinuId = function(id){
			that.porudzbina = {};
			abstractService.getOneId("porudzbina", id, function(resp,err){
				that.porudzbina = resp;
				//that.temp_istorije_porudzbine = resp.stavke;
				console.log(that.porudzbina);
				that.porudzbina.cena = 0;
				that.temp_istorije_porudzbine = resp.stavke;
				for (stavka of that.temp_istorije_porudzbine){
					that.porudzbina.cena = that.porudzbina.cena + parseFloat(stavka.cena);
				}
				console.log(that.porudzbina.cena);
				//that.istorijaPorudzbine(id);

			});
		}

		this.unesiPorudzbinu = function(){
			that.porudzbina.istorija = that.temp_istorije_porudzbine;
			that.porudzbina.radnik_id = that.radnik.id;
			abstractService.addOne("porudzbina", that.porudzbina, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('Porudzbina uneta!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniPorudzbinu = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    delete that.porudzbina.stavke;
		    console.log(that.porudzbina.podaci_dostave);
		    abstractService.editOne("porudzbina",that.porudzbina, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Porudzbina izmenjena!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}


		this.istorijaPorudzbine = function(id) {	
			that.temp_istorije_porudzbine = [];
			that.istorija_za_porudzbinu.porudzbina_id = id;
			abstractService.queryData("istorijaPorudzbine",that.istorija_za_porudzbinu, function(resp,err){
				if(resp){
					that.temp_istorije_porudzbine = resp;
					console.log(that.temp_istorije_porudzbine);
				}				
				

			});
		}

		this.obrisiIstorijuPorudzbine = function(id) {
			console.log('brisanje istorije')
			if(that.porudzbina.id){
					abstractService.deleteOne("istorijaPorudzbine",id, function(resp,err){
							if(resp){
								that.istorijaPorudzbine(that.porudzbina.id);
							}				
							console.log(resp);
							

						});
				

				

			}else{
				console.log('brisanje TEMP istorije')
				console.log(that.temp_istorije_porudzbine.length)
				for (var i=0; i<that.temp_istorije_porudzbine.length;i++){
					console.log(i);
					if(that.temp_istorije_porudzbine[i].id == id){

						that.temp_istorije_porudzbine.splice(i, 1);
					}
				}
			}

		}

		this.queryKorisnici = function() {
			abstractService.queryData("porudzbina",that.porudzbina, function(resp,err){				
				that.porudzbine = resp;
			});
		}

		this.init();
		
	}]);
})(angular);