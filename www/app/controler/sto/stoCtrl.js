(function (angular){

	var app = angular.module("app");


	app.controller('StoCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.sto = {};
		this.stolovi = [];
		this.deloviFirme = [];

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.sto = {};
				that.stolovi = [];
				that.deloviFirme = [];
				that.ucitajStolove();
				that.ucitajDeloveFirme();
			}else{
				$state.go('login');
			}

			
		};


		this.ucitajStoId = function(id){
			that.sto = {};
			abstractService.getOneId("sto", id, function(resp,err){
				that.sto = resp;
				console.log(that.sto);
			});

		};

		this.ucitajStolove = function(){
			that.stolovi = [];
			abstractService.getAll("sto",function(resp,err){
				console.log(resp);
				that.stolovi=resp;
			});

		};

		this.ucitajDeloveFirme = function(){
			that.deloviFirme = [];
			abstractService.getAll("deoFirme",function(resp,err){
				console.log(resp);
				that.deloviFirme=resp;
			});

		};


		this.obrisiSto = function(id){
			abstractService.deleteOne("sto", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Sto obrisan!');
					that.ucitajStolove();
				}
				
			});
		}


		this.unesiSto = function(){
			
			abstractService.addOne("sto", that.sto, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('Sto unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniSto = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("sto",that.sto, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Sto izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);