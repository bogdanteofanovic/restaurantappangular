(function (angular){

	var app = angular.module("app");


	app.controller('IstorijaPorudzbineCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];

		this.dejt = new Date('1980-01-01');
		this.today = new Date();
		this.istorija = {};
		this.istorije = [];
		this.radnici = [];
		this.stavke = [];
		this.suma = 0.0;
		this.istorija_query = {};

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.dejt = new Date('1980-01-01');
				that.today = new Date();
				that.istorija = {};
				that.istorije = [];
				that.radnici = [];
				that.stavke = [];
				that.suma = 0.0;
				that.istorija_query = {};
				that.ucitajIstorije();
				that.ucitajRadnike();
				that.ucitajStavke();

			}else{
				$state.go('login');
			}
		};


		

		this.pretrazi = function(){
			
			that.suma = 0.0;

			if(that.istorija.datum__from){
				that.istorija.datum__from = new Date(that.istorija.datum__from);
				that.istorija.datum__from.setDate(that.istorija.datum__from.getDate()+1);
				that.istorija.datum__from = that.istorija.datum__from.toISOString().substring(0,10);
			}
			if(that.istorija.datum__to){
				that.istorija.datum__to = new Date(that.istorija.datum__to);
				that.istorija.datum__to.setDate(that.istorija.datum__to.getDate()+1);
				that.istorija.datum__to = that.istorija.datum__to.toISOString().substring(0,10);
			}

			console.log(that.istorija);
			abstractService.queryData("istorijaPorudzbine",that.istorija,function(resp,err){
				
				console.log(resp);
				that.istorije=resp;
				if(that.istorije != null){
					for (i of that.istorije){
					that.suma = that.suma + parseFloat(i.cena);
				}
				}
				that.istorija_query = {};
				that.istorija = {};
			});
		}



		this.ucitajIstorije = function(){
			that.istorije = [];
			abstractService.getAll("istorijaPorudzbine",function(resp,err){
				that.istorije=resp;
				console.log(that.istorije);
				if(that.istorije != null){
					for (i of that.istorije){
					that.suma = that.suma + parseFloat(i.cena);
				}
				}
				
			});

		};

		this.ucitajRadnike = function(){
			that.radnici = [];
			abstractService.getAll("radnik",function(resp,err){
				that.radnici=resp;
			});

		};

		this.ucitajStavke = function(){
			that.stavke = [];
			abstractService.getAll("stavka",function(resp,err){
				that.stavke=resp;
			});

		};



		this.obrisiIstoriju = function(id){
			abstractService.deleteOne("istorijaPorudzbine", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Istorija obrisan!');
					that.ucitajIstorije();
				}
				
			});
		}




		this.init();
		
	}]);
})(angular);