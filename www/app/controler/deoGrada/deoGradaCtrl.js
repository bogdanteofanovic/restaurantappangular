(function (angular){

	var app = angular.module("app");


	app.controller('DeoGradaCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.deoGrada = {};
		this.deloviGrada = [];
		this.gradovi = [];


		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.deoGrada = {};
				that.deloviGrada = [];
				that.ucitajDeloveGrada();
				that.ucitajGradove();
			}else{
				$state.go('login');
			}

			
		};


		this.ucitajDeoGradaId = function(id){
			that.deoGrada = {};
			abstractService.getOneId("deoGrada", id, function(resp,err){
				that.deoGrada = resp;
				console.log(that.deoGrada);
			});

		};

		this.ucitajDeloveGrada = function(){
			that.deloviGrada = [];
			abstractService.getAll("deoGrada",function(resp,err){
				console.log(resp);
				that.deloviGrada=resp;
			});

		};

		this.ucitajGradove = function(){
			that.gradovi = [];
			abstractService.getAll("grad",function(resp,err){
				console.log(resp);
				that.gradovi=resp;
			});

		};


		this.obrisideoGrada = function(id){
			abstractService.deleteOne("deoGrada", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Deo grada obrisan!');
					that.ucitajDeloveGrada();
				}
				
			});
		}


		this.unesiDeoGrada = function(){
			
			abstractService.addOne("deoGrada", that.deoGrada, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('Deo grada unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniDeoGrada = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("deoGrada",that.deoGrada, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Deo grada izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);