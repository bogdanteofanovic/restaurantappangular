(function (angular){

	var app = angular.module("app");


	app.controller('FirmaCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.firma = {};
		this.firme = [];
		this.gradovi = [];

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.firma = {};
				that.firme = [];
				that.gradovi = [];
				that.ucitajFirme();
				that.ucitajGradove();
			}else{
				$state.go('login');
			}

			
		};


		this.ucitajFirmuId = function(id){
			that.firma = {};
			abstractService.getOneId("firma", id, function(resp,err){
				that.firma = resp;

				console.log(that.firma.grad.id);
			});

		};

		this.ucitajFirme = function(){
			that.firme = [];
			abstractService.getAll("firma",function(resp,err){
				console.log(resp);
				that.firme=resp;
			});

		};

		this.ucitajGradove = function(){
			that.gradovi = [];
			abstractService.getAll("grad",function(resp,err){
				console.log(resp);
				that.gradovi=resp;
			});

		};


		this.obrisiFrimu = function(id){
			abstractService.deleteOne("firma", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Deo firme obrisan!');
					that.ucitajFirme();
				}
				
			});
		}


		this.unesiFirmu = function(){
			
			abstractService.addOne("firma", that.firma, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('Deo firme unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniFirmu = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("firma",that.firma, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Deo firme izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);