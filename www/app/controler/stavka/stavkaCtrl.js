(function (angular){

	var app = angular.module("app");


	app.controller('StavkaCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.stavka = {aktivna:true};
		this.stavke = [];
		this.podkategorije = [];
		this.kategorije = [];
		this.fajl = {};
		this.fajlovi = [];
		this.query_data = {};


		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.stavka = {aktivna:true};
				that.stavke = [];
				that.podkategorije = [];
				that.kategorije = [];
				that.fajl = {};
				that.fajlovi = [];
				that.query_data = {};
				that.ucitajStavke();
				that.ucitajKategorije();
				that.ucitajPodkategorije();
			}else{
				$state.go('login');
			}

			
		};


		this.ucitajStavkuId = function(id){
			that.stavka = {};
			abstractService.getOneId("stavka", id, function(resp,err){
				that.stavka = resp;
				that.stavka.kategorija = that.stavka.podkategorija.kategorija;
				console.log(that.stavka);
			});

		};

		this.ucitajStavke = function(){
			that.stavke = [];
			abstractService.getAll("stavka",function(resp,err){
				console.log(resp);
				that.stavke=resp;
			});

		};

		this.ucitajKategorije = function(){
			that.kategorije = [];
			abstractService.getAll("kategorija",function(resp,err){
				console.log(resp);
				that.kategorije=resp;
			});

		};

		this.ucitajPodkategorije = function(){
			that.podkategorije = [];
			abstractService.getAll("podkategorija",function(resp,err){
				console.log(resp);
				that.podkategorije=resp;
			});

		};

		this.ucitajPodkategorijeZaIdKat = function(id){
			var katId = {"kategorija_id":null};
			katId.kategorija_id = id;
			console.log(that.stavka);
			that.podkategorije = [];
			abstractService.queryData("podkategorija",katId,function(resp,err){
				that.podkategorije = resp;
				if(err!=null){that.addAlert("Doslo je do greske: " + err);}
				
			});

		}


		this.obrisiStavku = function(id){
			abstractService.deleteOne("stavka", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Stavka obrisana!');
					that.ucitajStavke();
				}
				
			});
		}


		this.unesiStavku = function(){
			
			abstractService.addOne("stavka", that.stavka, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('Stavka uneta!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniStavku = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("stavka",that.stavka, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Stavka izmenjena!');
					
				}else{
					that.addAlert(err);
				}

		    });

		    
		}

		this.ucitajFajloveZaId = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    that.query_data.stavka_id = id;
		    abstractService.queryData("fajl",that.query_data, function(resp,err){				
		    	if(resp){
					
					that.fajlovi = resp;
					console.log('fajlovi');
					console.log(resp);
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}

		this.obrisiFajl = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.deleteOne("fajl", id, function(resp,err){				
		    	if(resp){
					that.ucitajFajloveZaId(that.query_data.stavka_id);
					that.addAlert('Stavka izmenjena!');
					
				}else{
					that.addAlert('Doslo je do greske kod brisanja!');
				}

		    });

		    
		}


		this.unesiFajl = function() {
		    //console.log(that.fajl.base64);
		    that.fajl.stavka_id = that.query_data.stavka_id;
		    console.log(that.stavka);
		    console.log(that.fajl);
		    abstractService.addOne("fajl",that.fajl, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Fajl unet');

					
				}else{
					that.addAlert('Doslo je do greske pri unosu fajla!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);