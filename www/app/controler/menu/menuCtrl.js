(function (angular){

	var app = angular.module("app");


	app.controller('MenuCtrl', ['$http','$scope','$state', '$window', '$rootScope', function($scope,$http,$state, $window, $rootScope){



		var that = this;

		this.notifikacije = [];

		this.stateName = $state;
		this.korisnik = null;
		this.loggedIn = false;
		this.radnik = {korisnicko_ime:'',lozinka:''};
		this.entity = 'radnik'

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){
			/*
			if($window.sessionStorage.getItem('restaurantapp_radnik_data')){
				$state.go('sve_porudzbine');
			}
			*/
			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.korisnik = JSON.parse(temp_korisnik);
				console.log(that.korisnik.korisnicko_ime);
				console.log('menu true');
				that.loggedIn=true;
			}else{
				console.log('menu false');
				
				that.loggedIn=false;
			}
			

		};

		


		this.logout = function(entity){
            
            $window.sessionStorage.removeItem('restaurantapp_radnik_data');
          	$state.go('login');
        };

		



		this.init();
		
	}]);
})(angular);