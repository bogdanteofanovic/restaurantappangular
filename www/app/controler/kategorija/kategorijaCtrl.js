(function (angular){

	var app = angular.module("app");


	app.controller('KategorijaCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.kategorija = {};
		this.kategorije = [];

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.kategorija = {};
				that.kategorije = [];
				that.ucitajKategorije();
			}else{
				$state.go('login');
			}
		};


		this.ucitajKategorijuId = function(id){
			that.kategorija = {};
			abstractService.getOneId("kategorija", id, function(resp,err){
				that.kategorija = resp;
			});

		};

		this.ucitajKategorije = function(){
			that.kategorije = [];
			abstractService.getAll("kategorija",function(resp,err){
				console.log(resp);
				that.kategorije=resp;
			});

		};

	


		this.obrisiKategoriju = function(id){
			abstractService.deleteOne("kategorija", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Kategorija obrisana!');
					that.ucitajKategorije();
				}
				
			});
		}


		this.unesiKategoriju = function(){
			
			abstractService.addOne("kategorija", that.kategorija, function(resp,err){
				console.log(that.kategorija);
				if(resp){
					that.init();
					that.addAlert('Kategorija uneta!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniKategoriju = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("kategorija",that.kategorija, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Kategorija izmenjena!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);