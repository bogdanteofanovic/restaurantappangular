(function (angular){

	var app = angular.module("app");


	app.controller('PodkategorijaCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.podkategorija = {};
		this.podkategorije = [];
		this.kategorije = [];
		

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.podkategorija = {};
				that.podkategorije = [];
				that.kategorije = [];
				that.ucitajPodkategorije();
				that.ucitajKategorije();
			}else{
				$state.go('login');
			}
			
			

			
		};


		this.ucitajPodkategorijuId = function(id){
			that.podkategorija = {};
			abstractService.getOneId("podkategorija", id, function(resp,err){
				that.podkategorija = resp;
			});

		};

		this.ucitajPodkategorije = function(){
			that.podkategorije = [];
			abstractService.getAll("podkategorija",function(resp,err){
				console.log(resp);
				that.podkategorije=resp;
			});

		};

		this.ucitajKategorije = function(){
			that.kategorije = [];
			abstractService.getAll("kategorija",function(resp,err){
				console.log(resp);
				that.kategorije=resp;
			});

		};



		this.obrisiPodkategoriju = function(id){
			abstractService.deleteOne("podkategorija", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Podkategorija obrisana!');
					that.ucitajPodkategorije();
				}
				
			});
		}


		this.unesiPodkategoriju = function(){
			
			abstractService.addOne("podkategorija", that.podkategorija, function(resp,err){
				console.log(that.podkategorija);
				if(resp){
					that.init();
					that.addAlert('Podkategorija uneta!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniPodkategoriju = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("podkategorija",that.podkategorija, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Podkategorija izmenjena!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}

		






		this.init();
		
	}]);
})(angular);