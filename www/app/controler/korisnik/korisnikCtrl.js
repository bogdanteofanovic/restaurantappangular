(function (angular){

	var app = angular.module("app");


	app.controller('KorisnikCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.korisnik = {};
		this.korisnici = [];
		this.gradovi = [];
		this.delovi_grada = [];


		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.korisnik = {};
				that.korisnici = [];
				that.gradovi = [];
				that.delovi_grada = [];
				that.ucitajKorisnike();
				that.ucitajGradove();
				that.ucitajDeloveGrada();
			}else{
				$state.go('login');
			}


			
		};

		this.ucitajKorisnike = function(){
			that.korisnici = [];
			abstractService.getAll("korisnik",function(resp,err){
				
				that.korisnici=resp;
				console.log('korisnici:',that.korisnici);
			});

		};


		this.ucitajKorisnikaId = function(id){
			that.korisnik = {};
			abstractService.getOneId("korisnik", id, function(resp,err){
				that.korisnik = resp;
				console.log(that.korisnik);
			});

		};

		this.ucitajGradove = function(){
			that.gradovi = [];
			abstractService.getAll("grad",function(resp,err){
				console.log(resp);
				that.gradovi=resp;
			});

		};

		this.ucitajDeloveGrada = function(){
			that.delovi_grada = [];
			abstractService.getAll("deoGrada",function(resp,err){
				console.log(resp);
				that.delovi_grada=resp;
			});

		};

		this.ucitajDeloveGradaZaId = function(id){
			that.delovi_grada = [];
			var temp_deo_grada = {'grad_id':id}
			abstractService.queryData("deoGrada",temp_deo_grada,function(resp,err){
				console.log(temp_deo_grada);
				console.log(resp);
				that.delovi_grada=resp;
			});

		};


		this.obrisiKorisnika = function(id){
			abstractService.deleteOne("korisnik", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('korisnik obrisan!');
					that.ucitajKorisnike();
				}
				
			});
		}


		this.unesiKorisnika = function(){
			
			abstractService.addOne("korisnik", that.korisnik, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('grad unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniKorisnika = function(id) {
		    //console.log(that.adresa.fajl.base64);

		    console.log(that.korisnik);
		    abstractService.editOne("korisnik",that.korisnik, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('korisnik izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);
