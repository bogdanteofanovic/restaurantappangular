(function (angular){

	var app = angular.module("app");


	app.controller('LoginCtrl', ['$http','$scope','abstractService','$state', '$window', '$rootScope', function($scope,$http,abstractService,$state, $window, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.radnik = {korisnicko_ime:'',lozinka:''};
		this.entity = 'radnik'

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				$state.go('sve_porudzbine');
			}else{
				
			}
		};

		this.login = function(){
			
			abstractService.addOne("radnik/login", that.radnik, function(resp,err){
				if(resp){
					console.log(resp);
					$window.sessionStorage.setItem('restaurantapp_radnik_data',JSON.stringify(resp));
					$state.go('sve_porudzbine');
					
				}else{
					that.addAlert('Neuspesna prijava!');
				}
				
			});
			
		}


		this.init();
		
	}]);
})(angular);