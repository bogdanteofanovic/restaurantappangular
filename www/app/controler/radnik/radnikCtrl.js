(function (angular){

	var app = angular.module("app");


	app.controller('RadnikCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];

		this.pretraga = false;
		this.radnik = {privilegije:{korisnik:1,administrator:0,menadzer:0}, aktivan:true};
		this.radnici = [];
		this.deloviFirme = [];

		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.radnik = {privilegije:{korisnik:1,administrator:0,menadzer:0}, aktivan:true};
				that.pretraga = false;
				that.radnici = [];
				that.deloviFirme = [];
				that.ucitajRadnike();
				that.ucitajDeloveFirme();
			}else{
				$state.go('login');
			}

			
		};

		this.pretragaa = function(){
			that.pretraga = true;
		}

		this.ucitajRadnikaId = function(id){
			that.radnik = {};
			abstractService.getOneId("radnik", id, function(resp,err){
				that.radnik = resp;
				console.log(id);
				console.log(that.radnik);
			});

		};

		this.ucitajRadnike = function(){
			that.radnici = [];
			abstractService.getAll("radnik",function(resp,err){
				console.log(resp);
				that.radnici=resp;
			});

		};

		this.ucitajDeloveFirme = function(){
			that.deloviFirme = [];
			abstractService.getAll("deoFirme",function(resp,err){
				console.log(resp);
				that.deloviFirme=resp;
			});

		};


		this.obrisiRadnika = function(id){
			abstractService.deleteOne("radnik", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Radnik obrisan!');
					that.ucitajRadnike();
				}
				
			});
		}


		this.unesiRadnika = function(){
			
			abstractService.addOne("radnik", that.radnik, function(resp,err){
				console.log(that.radnik);
				if(resp){
					that.init();
					that.addAlert('Radnik unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniRadnika = function(id) {
		    abstractService.editOne("radnik",that.radnik, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Radnik izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}

		this.pretraziRadnike = function() {
		    delete that.radnik.privilegije;
		    if(that.radnik.deoFirme){
		    	that.radnik.deoFirmeId = that.radnik.deoFirme;
		    	delete that.radnik.deoFirme;
		    }
		    abstractService.queryData("radnik",that.radnik, function(resp,err){				
		    	if(resp){
					that.radnici = [];
					that.radnici = resp;
					that.pretraga = false;
					that.radnik = {aktivan:true};
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);