(function (angular){

	var app = angular.module("app");


	app.controller('DeoFirmeCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.deoFirme = {};
		this.deloviFirme = [];


		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.deoFirme = {};
				that.deloviFirme = [];
				that.ucitajDeloveFirme();
			}else{
				$state.go('login');
			}


			
		};


		this.ucitajDeoFirmeId = function(id){
			that.deoFirme = {};
			abstractService.getOneId("deoFirme", id, function(resp,err){
				that.deoFirme = resp;
				console.log(that.deoFirme);
			});

		};

		this.ucitajDeloveFirme = function(){
			that.deloviFirme = [];
			abstractService.getAll("deoFirme",function(resp,err){
				console.log(resp);
				that.deloviFirme=resp;
			});

		};


		this.obrisiDeoFirme = function(id){
			abstractService.deleteOne("deoFirme", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('Deo firme obrisan!');
					that.ucitajDeloveFirme();
				}
				
			});
		}


		this.unesiDeoFirme = function(){
			
			abstractService.addOne("deoFirme", that.deoFirme, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('Deo firme unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniDeoFirme = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("deoFirme",that.deoFirme, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('Deo firme izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);