(function (angular){

	var app = angular.module("app");


	app.controller('GradCtrl', ['$http','$scope','abstractService', '$window','$state' ,'$rootScope', function($scope,$http,abstractService, $window, $state, $rootScope){



		var that = this;

		this.notifikacije = [];


		this.grad = {};
		this.gradovi = [];


		this.addAlert = function(message) {
			that.notifikacije.push({msg:message});
		};

		this.closeAlert = function(index) {
			that.notifikacije.splice(index, 1);

		};


		this.init = function(){

			

			var temp_korisnik = $window.sessionStorage.getItem('restaurantapp_radnik_data');
			if(temp_korisnik){
				that.grad = {};
				that.gradovi = [];
				that.ucitajGradove();
			}else{
				$state.go('login');
			}


			
		};


		this.ucitajGradId = function(id){
			that.grad = {};
			abstractService.getOneId("grad", id, function(resp,err){
				that.grad = resp;
				console.log(that.grad);
			});

		};

		this.ucitajGradove = function(){
			that.gradovi = [];
			abstractService.getAll("grad",function(resp,err){
				console.log(resp);
				that.gradovi=resp;
			});

		};


		this.obrisiGrad = function(id){
			abstractService.deleteOne("grad", id, function(resp,err){				
				
				if(err!=null){
					console.log(err);
					that.addAlert("Doslo je do greske: " + err.message);
				}else{
					that.addAlert('grad obrisan!');
					that.ucitajGradove();
				}
				
			});
		}


		this.unesiGrad = function(){
			
			abstractService.addOne("grad", that.grad, function(resp,err){
				if(resp){
					that.init();
					that.addAlert('grad unet!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}
				
			});
			
		}

		this.izmeniGrad = function(id) {
		    //console.log(that.adresa.fajl.base64);
		    abstractService.editOne("grad",that.grad, id, function(resp,err){				
		    	if(resp){
					that.init();
					that.addAlert('grad izmenjen!');
					
				}else{
					that.addAlert('Doslo je do greske!');
				}

		    });

		    
		}



		this.init();
		
	}]);
})(angular);