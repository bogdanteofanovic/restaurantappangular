from functools import wraps
from flask import request, session, jsonify, current_app
import jwt


def secured(roles=[]):
    def secured_with_roles(f):
        @wraps(f)
        def login_check(*args, **kwargs):
            if 'X-API-KEY' in request.headers:
                token = request.headers['X-API-KEY']
                try:
                    data = jwt.decode(token, current_app.config['SECRET_KEY'])
                except:
                    print('nevalidan token')
                    return 'Token nije validan', 420
                if data['roles'] is not None:
                    for role in data['roles']:
                        if role in roles:
                            return f(*args, **kwargs)

                    return 'Nemate prava pristupa', 403

            print('nevalidan token2')
            return "test", 420

        login_check.__doc__ = f.__doc__
        login_check.__name__ = f.__name__
        return login_check
    return secured_with_roles
