from service.abstract_service import AbstractService
from model.firma_entity import Firma


def get_firma():
    return AbstractService().get_entity_for_id(Firma,1)
