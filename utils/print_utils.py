from flask import Flask, render_template, make_response, request
import pdfkit

def print_porudzbina():

    rendered = render_template('/print/porudzbina_pdf.html',data = request.get_json(force=True))
    pdf = pdfkit.from_string(rendered, False)

    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachment; filename = output.pdf'

    return response
