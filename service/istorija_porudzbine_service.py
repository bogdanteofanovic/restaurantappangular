from model.korisnik_entity import Korisnik
from model.istorija_porudzbine_entity import IstorijaPorudzbine
from model.radnik_entity import Radnik
from service.abstract_service import AbstractService
from service.api import istorija_porudzbine_ns
from flask import request
from model.stavka_entity import Stavka
from model.porudzbina_entity import Porudzbina
from service.abstract_service import Session
from operator import attrgetter
from utils.login_utils import secured
from datetime import datetime

@istorija_porudzbine_ns.route('/<int:id>')
@istorija_porudzbine_ns.route('/')
class IstorijaPorudzbineService(AbstractService):
    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        result = []

        if id is None:

            for r in sorted(super().get_all_entities(IstorijaPorudzbine), key=attrgetter('datum')):
                result.append(super().deep_json_from_attributes(r))
            return result

        elif isinstance(id, int):
            return super().deep_json_from_attributes(super().get_entity_for_id(IstorijaPorudzbine, id))

    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)
        temp_stavka = super().get_entity_for_id(Stavka, entity['stavka_id'])
        temp_radnik = super().get_entity_for_id(Radnik, entity['radnik_id'])
        temp_porudzbina = super().get_entity_for_id(Porudzbina, entity['porudzbina_id'])
        temp_istorija = IstorijaPorudzbine(temp_stavka, temp_porudzbina, temp_radnik, None, datetime.now().isoformat())
        super().add_one(temp_istorija)
        return "Uspesno dodat entitet", 201

    def put(self,id):


        return "Nije moguce menjati", 308

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def delete(self,id):
        super().delete_one(IstorijaPorudzbine,id)
        return "Uspesno obrisana", 201


@istorija_porudzbine_ns.route('/query/')
@istorija_porudzbine_ns.route('/query/<id>')
class IstorijaPorudzbineQuery(AbstractService):

    def get(self,id):

        result = []
        if id == "aktivne":
            print("tst")
            for r in self.get_all_aktivne():
                print(r)
                result.append(super().deep_json_from_attributes(r))
            return result
        else:
            return "Greska",404

    def post(self):

        result = []
        entity = request.get_json(force=True)
        print(entity)

        for r in super().query_data(IstorijaPorudzbine, entity):
           result.append(super().deep_json_from_attributes(r))

        return result
