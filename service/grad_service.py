from service.abstract_service import AbstractService
from model.grad_entity import Grad
from service.api import grad_ns
from flask import request
from utils.login_utils import secured

@grad_ns.route('/')
@grad_ns.route('/<int:id>')
class GradService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(Grad):
                one = one.asdict()
                all.append(one)
            return all
        elif isinstance(id,int):
            return super().get_entity_for_id(Grad, id).asdict()

    @secured(roles=['menadzer', 'administrator'])
    def post(self):

        entity = request.get_json(force=True)
        super().add_one(Grad(entity["naziv"],entity["postanski_broj"]))

        return "Uspesno dodat",201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        temp_grad = super().get_entity_for_id(Grad,id)
        entity = request.get_json(force=True)
        temp_grad.naziv = entity["naziv"]
        temp_grad.postanski_broj = entity["postanski_broj"]
        super().merge_one(temp_grad)
        return "Uspesno izmenjen grad", 201

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(Grad,id)



