from flask import Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type':'apiKey',
        'in' : 'header',
        'name' : 'X-API-KEY'
    }
}

apiv1 = Blueprint('api', __name__, url_prefix='/v1')



api = Api(apiv1, version='1.0', title='RestaurantApp API',
          description='REST points', authorizations=authorizations)

adresa_ns = api.namespace('adresa', 'Adresa service metode')
grad_ns = api.namespace('grad', 'Grad service metode')
deo_grada_ns = api.namespace('deoGrada', 'Deo grada service metode')
korisnik_ns = api.namespace('korisnik', 'Korisnik service metode')
firma_ns = api.namespace('firma', 'Firma service metode')
deo_firme_ns = api.namespace('deoFirme', 'Deo firme service metode')
istorija_porudzbine_ns = api.namespace('istorijaPorudzbine', 'Istorija porudzbine service metode')
kategorija_ns = api.namespace('kategorija', 'Kategorija service metode')
podkategorija_ns = api.namespace('podkategorija', 'Podkategorija service metode')
porudzbina_ns = api.namespace('porudzbina', 'Porudzbina service metode')
privilegija_ns = api.namespace('privilegija', 'Privilegija service metode')
radnik_ns = api.namespace('radnik', 'Radnik service metode')
stavka_ns = api.namespace('stavka', 'Stavka service metode')
sto_ns = api.namespace('sto', 'Sto service metode')
status_ns = api.namespace('status', 'Status service metode')
fajl_ns = api.namespace('fajl', 'Fajl service metode')



