from model.korisnik_entity import Korisnik
from model.grad_entity import Grad
from model.deo_grada_entity import DeoGrada
from service.abstract_service import AbstractService
from service.api import korisnik_ns
from flask import request
from werkzeug.security import generate_password_hash,check_password_hash
from utils.login_utils import secured


@korisnik_ns.route('/<int:id>')
@korisnik_ns.route('/')
class KorisnikService(AbstractService):
    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(Korisnik):
                one = one.asdict()
                del one["deo_grada_id"], one["lozinka"], one["ulica"],one["stan"],one["sprat"]
                all.append(one)
            return all
        elif isinstance(id, int):
            one = super().get_entity_for_id(Korisnik, id).asdict()
            one["deo_grada"] = super().get_entity_for_id(DeoGrada,one["deo_grada_id"]).asdict()
            del one["deo_grada_id"]
            one["grad"] = super().get_entity_for_id(Grad,one["deo_grada"]["grad_id"]).asdict()
            del one["deo_grada"]["grad_id"]
            return one

    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)
        deo_grada = super().get_entity_for_id(DeoGrada, entity["deo_grada"])
        if "lozinka" not in entity:
            lozinka_hashed = generate_password_hash('restaurantapp2019', method='sha256')
        else:
            lozinka_hashed = generate_password_hash(entity['lozinka'], method='sha256')

        korisnik = Korisnik(entity['ime'], entity['prezime'], entity['telefon'], entity['email'], entity['email'], lozinka_hashed, entity['ulica'], entity['stan'], entity['sprat'] ,deo_grada, entity['napomena'])
        super().add_one(korisnik)
        return "Uspesno dodat", 201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):
        temp_korisnik = super().get_entity_for_id(Korisnik, id)
        entity = request.get_json(force=True)
        print(entity)
        temp_korisnik.ime = entity['ime']
        temp_korisnik.prezime = entity['prezime']
        temp_korisnik.telefon = entity['telefon']
        temp_korisnik.email = entity['email']
        temp_korisnik.ulica = entity['ulica']
        temp_korisnik.stan = entity['stan']
        temp_korisnik.sprat = entity['sprat']
        temp_korisnik.napomena = entity['napomena']
        gr = entity["deo_grada"]
        if isinstance(gr,int):
            deo_grada = super().get_entity_for_id(DeoGrada, gr)
        else:
            deo_grada = super().get_entity_for_id(DeoGrada, gr["id"])
        temp_korisnik.deo_grada = deo_grada
        super().add_one(temp_korisnik)
        return "Uspesno izmenjen entitet", 201

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(Korisnik,id)


@korisnik_ns.route('/query/')
@korisnik_ns.route('/query/<id>')
class KorisnikQuery(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):

        result = []
        print(request.get_json(force=True))

        for r in super().query_data(Korisnik, request.get_json(force=True)):
            result.append(super().deep_json_from_attributes(r))

        return result

@korisnik_ns.route('/porudzbina/')
class KorisnikPorudzbina(AbstractService):

    #@secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):

        result = []
        print(request.get_json(force=True))



        return "Okej", 201