from model.firma_entity import Firma
from service.abstract_service import AbstractService
from service.api import firma_ns
from flask import request
from model.grad_entity import Grad
from utils.login_utils import secured

@firma_ns.route('/<int:id>')
@firma_ns.route('/')
class FirmaService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        try:
            if id is None:
                all = []
                for one in super().get_all_entities(Firma):
                    one = one.asdict()
                    one['grad'] = super().get_entity_for_id(Grad, one['gradId']).asdict()
                    del one['gradId']
                    all.append(one)
                return all
            elif isinstance(id, int):
                one = super().get_entity_for_id(Firma, id).asdict()
                one['grad'] = super().get_entity_for_id(Grad, one['gradId']).asdict()
                del one['gradId']
                return one
        except:

            return "Doslo je do greske", 500

    def post(self):

        return "Nije moguće dodati firmu", 500

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        entity = request.get_json(force=True)
        temp_firma = super().get_entity_for_id(Firma, id)
        temp_firma.naziv = entity["naziv"]
        temp_firma.kontakt_telefon = entity["kontakt_telefon"]
        temp_firma.kontakt_email = entity["kontakt_email"]
        temp_firma.web_adresa = entity["web_adresa"]
        temp_firma.ulica = entity["ulica"]
        temp_firma.broj = entity["broj"]
        print(entity["grad"])
        if isinstance(entity["grad"], int):
            temp_grad = super().get_entity_for_id(Grad,entity["grad"])
        else:
            temp_grad = super().get_entity_for_id(Grad, entity["grad"]["id"])
        temp_firma.grad = temp_grad
        super().merge_one(temp_firma)

        return "Uspešno izmenjena firma", 201

    def delete(self,id):
        return  "Nije moguće obrisati firmu", 500

