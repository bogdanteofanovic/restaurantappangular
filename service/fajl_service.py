from model.fajl_entity import Fajl
from model.stavka_entity import Stavka
from service.abstract_service import AbstractService
from service.api import fajl_ns
from flask import request
from utils.login_utils import secured


@fajl_ns.route('/')
@fajl_ns.route('/<int:id>')
class FajlService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self,id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(Fajl):

                one = one.asdict()

                all.append(one)
            return all

        elif isinstance(id,int):
            one = super().get_entity_for_id(Fajl, id)
            return one

    def put(self,id):

        return "Nije dozvoljena izmena", 401

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(Fajl,id)
        return "Uspesno obrisana", 201

    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)
        print(entity)
        temp_stavka = super().get_entity_for_id(Stavka, entity['stavka_id'])
        temp_fajl = Fajl(entity['base64'],temp_stavka)
        super().add_one(temp_fajl)
        return "Uspesno dodat", 201


@fajl_ns.route('/query/')
class FajlQuery(AbstractService):
    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):
        result = []
        for r in super().query_data(Fajl, request.get_json(force=True)):

            r = r.asdict()

            result.append(r)
        return result
