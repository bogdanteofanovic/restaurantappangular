from model.firma_entity import Firma
from model.sto_entity import Sto
from service.abstract_service import AbstractService
from service.api import sto_ns
from flask import request
from model.deo_firme_entity import DeoFirme
from service.abstract_service import Session
from utils.login_utils import secured

@sto_ns.route('/<int:id>')
@sto_ns.route('/')
class StoService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(Sto):
                one = one.asdict()
                one['deoFirme'] = super().get_entity_for_id(DeoFirme, one['deoFirmeId']).asdict()
                del one['deoFirmeId']
                all.append(one)
            return all
        elif isinstance(id, int):
            one = super().get_entity_for_id(Sto, id).asdict()
            one['deoFirme'] = super().get_entity_for_id(DeoFirme, one['deoFirmeId']).asdict()
            del one['deoFirmeId']
            return one

    @secured(roles=['menadzer', 'administrator'])
    def post(self):

        entity = request.get_json(force=True)
        temp_deo_firme = super().get_entity_for_id(DeoFirme,entity["deoFirme"])
        temp_sto = Sto(entity["naziv"], temp_deo_firme)
        super().add_one(temp_sto)

        return "Uspesno dodat", 201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        temp_sto = super().get_entity_for_id(Sto, id)
        entity = request.get_json(force=True)
        temp_sto.naziv = entity["naziv"]
        if isinstance(entity["deoFirme"], int):
            temp_deo_firme = super().get_entity_for_id(DeoFirme,entity["deoFirme"])
            temp_sto.deoFirme = temp_deo_firme
        else:
            temp_deo_firme = super().get_entity_for_id(DeoFirme,entity["deoFirme"]["id"])
            temp_sto.deoFirme = temp_deo_firme
        super().merge_one(temp_sto)

        return "Uspesno izmenjen entitet", 201

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(Firma,id)
