from model.radnik_entity import Radnik
from model.privilegija_entity import Privilegija
from model.grad_entity import Grad
from model.deo_firme_entity import DeoFirme
from service.abstract_service import AbstractService
from service.api import radnik_ns
from flask import request, current_app
import jwt
import datetime
from utils.login_utils import secured

from werkzeug.security import generate_password_hash,check_password_hash


@radnik_ns.route('/')
@radnik_ns.route('/<int:id>')
class RadnikService(AbstractService):
    @secured(roles=['administrator','menadzer'])
    def get(self,id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(Radnik):

                one = one.asdict()

                all.append(one)
            return all

        elif isinstance(id,int):
            one = super().get_entity_for_id(Radnik, id)
            privilegije = one.privilegije[0].asdict()
            one = one.asdict()
            one['deoFirme'] = super().get_entity_for_id(DeoFirme, one['deoFirmeId']).asdict()
            del one['deoFirmeId']
            one["privilegije"] = privilegije
            print(one)
            return one

    @secured(roles=['administrator','menadzer'])
    def put(self,id):
        try:
            temp_radnik = super().get_entity_for_id(Radnik,id)
            entity = request.get_json(force=True)
            temp_radnik.ime = entity["ime"]
            temp_radnik.prezime = entity["prezime"]
            temp_radnik.email = entity["email"]
            temp_radnik.telefon = entity["telefon"]
            temp_radnik.korisnicko_ime = entity["korisnicko_ime"]
            if entity["lozinka"] == temp_radnik.lozinka:
                pass
            else:
                lozinka_hashed = generate_password_hash(entity['lozinka'], method='sha256')
                temp_radnik.lozinka = lozinka_hashed

            temp_radnik.aktivan = entity["aktivan"]
            if isinstance(entity["deoFirme"], int):
                temp_deo_firme = super().get_entity_for_id(DeoFirme,entity["deoFirme"])
                temp_radnik.deoFirme = temp_deo_firme
            else:
                temp_deo_firme = super().get_entity_for_id(DeoFirme,entity["deoFirme"]["id"])
                temp_radnik.deoFirme = temp_deo_firme
            super().merge_one(temp_radnik)
            temp_privilegije = super().get_entity_for_id(Privilegija,entity["privilegije"]["id"])
            temp_privilegije.korisnik = entity["privilegije"]["korisnik"]
            temp_privilegije.menadzer = entity["privilegije"]["menadzer"]
            temp_privilegije.administrator = entity["privilegije"]["administrator"]

            super().merge_one(temp_privilegije)
            return "Uspesno izmenjen entitet", 201
        except:
            return "Doslo je do greske", 403





    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(Radnik,id)

    @secured(roles=['menadzer','administrator'])
    def post(self):
        try:
            entity = request.get_json(force=True)
            lozinka_hashed = generate_password_hash(entity['lozinka'], method='sha256')
            print(entity)
            temp_deo_firme = super().get_entity_for_id(DeoFirme,entity["deoFirme"])
            temp_radnik = Radnik(entity["ime"], entity["prezime"], entity["email"], entity["telefon"], entity["korisnicko_ime"], lozinka_hashed, temp_deo_firme)
            super().add_one(temp_radnik)
            temp_privilegija = Privilegija(temp_radnik,entity["privilegije"]["korisnik"], entity["privilegije"]["administrator"], entity["privilegije"]["menadzer"])
            super().add_one(temp_privilegija)
            return "Uspesno dodat", 201
        except:
            return "Doslo je do greske", 403


@radnik_ns.route('/query/')
class RadnikQuery(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)
        print(entity)
        result = []
        for r in super().query_data(Radnik, entity):
            privilegije = r.privilegije[0].asdict()
            r = r.asdict()
            r["privilegije"] = privilegije
            result.append(r)
        return result


@radnik_ns.route('/login/')
class RadnikLogin(AbstractService):

    def post(self):
        entity = request.get_json(force=True)
        print(entity)
        if 'korisnicko_ime' in entity:
            result = super().query_data(Radnik, {'korisnicko_ime': entity['korisnicko_ime']})
            print(result)
            temp_radnik = super().get_entity_for_id(Radnik, result[0].id)
            if temp_radnik.aktivan:
                roles = []
                if check_password_hash(temp_radnik.lozinka, entity['lozinka']):
                    for privilegija in temp_radnik.privilegije:
                        for k, v in vars(privilegija).items():
                            print(k,' ',v)
                            if v is True:
                                roles.append(k)
                    print(roles)
                    token = jwt.encode({'user': temp_radnik.korisnicko_ime,
                                        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30), 'roles':roles}, current_app.config['SECRET_KEY'])
                    return {'token': token.decode('UTF-8'), 'korisnicko_ime': entity['korisnicko_ime'], 'id': temp_radnik.id}
            else:
                return 'Pogresni podaci za prijavu 1', 401
        return ''