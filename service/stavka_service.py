from model.korisnik_entity import Korisnik
from model.stavka_entity import Stavka
from model.podkategorija_entity import PodKategorija
from service.abstract_service import AbstractService
from service.api import stavka_ns
from flask import request
from operator import attrgetter
from utils.login_utils import secured






@stavka_ns.route('/<int:id>')
@stavka_ns.route('/')
class StavkaService(AbstractService):

    #@secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        result = []
        print('ID get',id)
        if id is None:
            for r in sorted(super().get_all_entities(Stavka), key=attrgetter('naziv')):
                result.append(super().deep_json_from_attributes(r))

            return result
        elif isinstance(id, int):
            return super().deep_json_from_attributes(super().get_entity_for_id(Stavka, id))
        else:
            return "Stavka nije pronadjena", 404

    @secured(roles=['menadzer', 'administrator'])
    def post(self):

        entity = request.get_json(force=True)
        print(entity)
        temp_podkategorija = super().get_entity_for_id(PodKategorija, entity["podkategorija"])
        temp_stavka = Stavka(entity["naziv"], entity["opis"], entity["tezina"], entity["cena"], temp_podkategorija, entity["aktivna"])
        super().add_one(temp_stavka)

        return "Uspesno dodat", 201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):
        try:
            print('ID put', id)
            temp_stavka = super().get_entity_for_id(Stavka, id)
            entity = request.get_json(force=True)
            print(entity)
            if isinstance(entity["podkategorija"], int):
                temp_podkategorija = super().get_entity_for_id(PodKategorija, entity["podkategorija"])
                temp_stavka.podkategorija = temp_podkategorija
            else:
                temp_podkategorija = super().get_entity_for_id(PodKategorija, entity["podkategorija"]["id"])
                temp_stavka.podkategorija = temp_podkategorija

            temp_stavka.naziv = entity["naziv"]
            temp_stavka.opis = entity["opis"]
            temp_stavka.tezina = entity["tezina"]
            temp_stavka.cena = entity["cena"]
            temp_stavka.aktivna = entity["aktivna"]
            temp_stavka.podkategorija = temp_podkategorija
            super().merge_one(temp_stavka)
        except:
            return "Doslo je do greske", 403

        return "Uspesno izmenjen entitet", 201

    @secured(roles=['administrator'])
    def delete(self,id):

        super().delete_one(Stavka,id)

        return "Uspesno obrisana stavka", 201


@stavka_ns.route('/query/')
class StavkaQuery(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):
        result = []
        for r in super().query_data(Stavka, request.get_json(force=True)):
            result.append(super().deep_json_from_attributes(r))

        return result