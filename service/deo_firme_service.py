from service.abstract_service import AbstractService
from service.api import deo_firme_ns
from flask import request
from model.firma_entity import Firma
from model.deo_firme_entity import DeoFirme
from utils.login_utils import secured

@deo_firme_ns.route('/<int:id>')
@deo_firme_ns.route('/')
class DeoFirmeService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        if id is None:
            all = []
            for one in super().get_all_full_json(DeoFirme):

                all.append(one)
            return all
        elif isinstance(id, int):
            one = super().get_entity_for_id(DeoFirme, id).asdict()

            return one

    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)

        temp_firma = super().get_entity_for_id(Firma,1)
        temp_deo_firme = DeoFirme(entity["naziv"],temp_firma)
        super().add_one(temp_deo_firme)

        return "Uspesno dodat", 201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):
        temp_deo_firme = super().get_entity_for_id(DeoFirme,id)
        temp_deo_firme.naziv = request.get_json(force=True)["naziv"]
        super().merge_one(temp_deo_firme)
        return "Uspesno izmenjen entitet", 201

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(DeoFirme,id)


@deo_firme_ns.route('/query')
class KorisnikQuery(AbstractService):
    def get(self):

        return None