from service.abstract_service import AbstractService
from model.deo_grada_entity import DeoGrada
from model.grad_entity import Grad
from service.api import deo_grada_ns
from flask import request
from utils.login_utils import secured


@deo_grada_ns.route('/')
@deo_grada_ns.route('/<int:id>')
class DeoGradService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(DeoGrada):
                all.append(super().deep_json_from_attributes(one))

            return all
        elif isinstance(id,int):
            entity = super().get_entity_for_id(DeoGrada, id).asdict()
            entity['grad'] = super().get_entity_for_id(Grad,entity['grad_id']).asdict()
            return entity

    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)
        temp_grad = super().get_entity_for_id(Grad,entity["grad"]["id"])
        super().add_one(DeoGrada(entity["naziv"], temp_grad))
        return "Uspesno dodat",201

    @secured(roles=['menadzer', 'administrator'])
    def put(self, id):
        entity = request.get_json(force=True)
        temp_entity = super().get_entity_for_id(DeoGrada,id)
        temp_entity.naziv = entity["naziv"]
        super().merge_one(temp_entity)
        return "Uspesno dodat", 201

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(DeoGrada,id)


@deo_grada_ns.route('/query/')
@deo_grada_ns.route('/query/<id>')
class KorisnikQuery(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):

        result = []
        print(request.get_json(force=True))

        for r in super().query_data(DeoGrada, request.get_json(force=True)):
            result.append(super().deep_json_from_attributes(r))

        return result

