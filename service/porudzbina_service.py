from model.porudzbina_entity import Porudzbina
from model.istorija_porudzbine_entity import IstorijaPorudzbine
from model.stavka_entity import Stavka
from model.korisnik_entity import Korisnik
from model.deo_grada_entity import DeoGrada
from model.sto_entity import Sto
from model.status_entity import StatusPorudzbine
from model.radnik_entity import Radnik
from service.abstract_service import AbstractService
from service.api import porudzbina_ns
from flask import request, make_response, render_template
from datetime import datetime
import utils.email_utils as email_utils
import pdfkit
from os.path import join, dirname
import os
from utils.login_utils import secured
from operator import attrgetter


@porudzbina_ns.route('/')
@porudzbina_ns.route('/<int:id>')
class PorudzbinaService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self,id=None):

        result = []
        temp_stavke = []
        if id is None:
            for r in sorted(super().get_all_entities(Porudzbina), key=attrgetter('datum_otvaranja')):
                entity = super().deep_json_from_attributes(r)

                result.append(entity)


            return result

        elif isinstance(id,int):
            entity = super().deep_json_from_attributes(super().get_entity_for_id(Porudzbina, id))
            entity["stavke"] = []

            if not entity['dostava'] or entity['licno_preuzimanje'] or entity['online']:
                entity['dostavljanje'] = {'id':'restoran','naziv':'U restoranu'}
                entity['sto']={'id':entity['sto_id']}

            if entity['dostava']:
                entity['dostavljanje'] = {'id':'dostava','naziv':'Dostava'}

            if entity['licno_preuzimanje']:
                entity['dostavljanje'] = {'id': 'licno', 'naziv': 'Lično'}

            for stavka in eval(entity["istorija_porudzbine"]):
                entity["stavke"].append(super().deep_json_from_attributes(super().get_entity_for_id(IstorijaPorudzbine, int(stavka))))
            del entity["istorija_porudzbine"]

            return entity

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        entity = request.get_json(force=True)
        temp_porudzbina = super().get_entity_for_id(Porudzbina,id)
        print(entity)

        try:



            if 'status' in entity:
                if entity['status'] == 'priprema':
                    temp_porudzbina.status_porudzbine = super().query_data(StatusPorudzbine, {"status":"priprema"})[0]
                    temp_porudzbina.ukupna_cena = 0
                    temp_porudzbina.datum_zatvaranja = None
                    super().merge_one(temp_porudzbina)
                    return 'uspesno izmenjen status',201

                if entity['status'] == 'isporucena':
                    temp_porudzbina.status_porudzbine = super().query_data(StatusPorudzbine, {"status": "isporucena"})[0]
                    temp_suma = 0.0
                    for istorija in temp_porudzbina.istorija_porudzbine:
                        temp_suma = temp_suma + istorija.cena
                    temp_porudzbina.ukupna_cena = temp_suma
                    super().merge_one(temp_porudzbina)
                    return 'uspesno izmenjen status', 201

                if entity['status'] == 'zatvorena':
                    temp_porudzbina.status_porudzbine = super().query_data(StatusPorudzbine, {"status": "zatvorena"})[0]
                    temp_porudzbina.datum_zatvaranja  = datetime.now().isoformat()
                    temp_suma = 0.0
                    for istorija in temp_porudzbina.istorija_porudzbine:
                        temp_suma = temp_suma + istorija.cena
                    temp_porudzbina.ukupna_cena = temp_suma
                    super().merge_one(temp_porudzbina)
                    return 'uspesno izmenjen status', 201

                if entity['status'] == 'naplacena':
                    temp_porudzbina.status_porudzbine = super().query_data(StatusPorudzbine, {"status": "naplacena"})[0]
                    temp_suma = 0.0
                    if temp_porudzbina.datum_zatvaranja is None:
                        temp_porudzbina.datum_zatvaranja = datetime.now().isoformat()
                    for istorija in temp_porudzbina.istorija_porudzbine:
                        temp_suma = temp_suma + istorija.cena
                    temp_porudzbina.ukupna_cena = temp_suma
                    super().merge_one(temp_porudzbina)
                    return 'uspesno izmenjen status', 201

            if not "napomena" in entity:
                entity["napomena"] = ""
            else:
                temp_porudzbina.napomena = entity['napomena']

            if entity['dostavljanje']=='licno':
                temp_porudzbina.licno_preuzimanje = True
                temp_porudzbina.korisnik = None
                temp_porudzbina.dostava = False
                temp_porudzbina.dostava_ime = None
                temp_porudzbina.dostava_prezime = None
                temp_porudzbina.dostava_telefon = None
                temp_porudzbina.dostava_ulica = None
                temp_porudzbina.dostava_stan = None
                temp_porudzbina.dostava_sprat = None
                temp_porudzbina.dostava_deo_grada_id = None

            if entity['dostavljanje'] == 'dostava':
                print('dostava if')
                temp_porudzbina.licno_preuzimanje = False
                temp_porudzbina.korisnik = None
                temp_porudzbina.dostava_ime = entity['podaci_dostave']['ime']
                temp_porudzbina.dostava_prezime = entity['podaci_dostave']['prezime']
                temp_porudzbina.dostava_telefon = entity['podaci_dostave']['telefon']
                temp_porudzbina.dostava_ulica = entity['podaci_dostave']['ulica']
                temp_porudzbina.dostava_stan = entity['podaci_dostave']['stan']
                temp_porudzbina.dostava_sprat = entity['podaci_dostave']['sprat']
                temp_porudzbina.dostava_deo_grada = super().get_entity_for_id(DeoGrada,int(entity['podaci_dostave']['deo_grada']))
                temp_porudzbina.dostava = True

            if entity['dostavljanje'] == 'restoran':
                temp_porudzbina.licno_preuzimanje = False
                temp_porudzbina.korisnik = None
                temp_porudzbina.dostava = False
                temp_porudzbina.dostava_ime = None
                temp_porudzbina.dostava_prezime = None
                temp_porudzbina.dostava_telefon = None
                temp_porudzbina.dostava_ulica = None
                temp_porudzbina.dostava_stan = None
                temp_porudzbina.dostava_sprat = None
                temp_porudzbina.dostava_deo_grada_id = None

                temp_sto = super().get_entity_for_id(Sto, entity['sto'])
                temp_porudzbina.sto = temp_sto

            super().merge_one(temp_porudzbina)


            if 'dopuna_istorije' in entity:

                for istorija in entity["dopuna_istorije"]:
                    temp_stavka = super().get_entity_for_id(Stavka, int(istorija["stavka"]["id"]))
                    print(temp_stavka.naziv)
                    temp_radnik = super().get_entity_for_id(Radnik, int(istorija["radnik_id"]))
                    print(temp_radnik.ime)
                    temp_istorija = IstorijaPorudzbine(temp_stavka, temp_porudzbina, temp_radnik, None, datetime.now().isoformat())
                    print(temp_istorija.stavka.naziv)
                    print(temp_istorija.id)
                    super().merge_one(temp_istorija)


            return "Uspesno izmenjen entitet", 201
        except:
            return "Doslo je do greske prilikom izmene", 405

    @secured(roles=['administrator'])
    def delete(self,id):

       super().delete_one(Porudzbina,id)


    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        try:
            entity = request.get_json(force=True)
            temp_korisnik = None
            temp_licno = False
            temp_dostava = False
            print(entity)

            if not "napomena" in entity:
                entity["napomena"] = ""


            if entity['dostavljanje']=='licno':
                temp_licno = True

            status_otvorena = super().query_data(StatusPorudzbine, {"status": "otvorena"})[0]
            temp_radnik = super().get_entity_for_id(Radnik, entity['radnik_id'])
            temp_porudzbina = Porudzbina(status_otvorena, datetime.now().isoformat(), entity["napomena"],dostava=temp_dostava, licno_preuzimanje=temp_licno, radnik=temp_radnik)

            porudzbina = super().add_one(temp_porudzbina)

            for istorija in entity["istorija"]:
                temp_stavka = super().get_entity_for_id(Stavka, int(istorija["stavka"]["id"]))
                temp_radnik = super().get_entity_for_id(Radnik, entity['radnik_id'])
                temp_istorija = IstorijaPorudzbine(temp_stavka, porudzbina, temp_radnik, None, datetime.now().isoformat())
                super().add_one(temp_istorija)

            if entity['dostavljanje'] == 'dostava':

                print(entity['podaci_dostave'])
                porudzbina.dostava_ime = entity['podaci_dostave']['ime']
                porudzbina.dostava_prezime = entity['podaci_dostave']['prezime']
                porudzbina.dostava_telefon = entity['podaci_dostave']['telefon']
                porudzbina.dostava_ulica = entity['podaci_dostave']['ulica']
                porudzbina.dostava_stan = entity['podaci_dostave']['stan']
                porudzbina.dostava_sprat = entity['podaci_dostave']['sprat']
                porudzbina.dostava_deo_grada = super().get_entity_for_id(DeoGrada, int(entity['podaci_dostave']['deo_grada']))
                porudzbina.dostava = True
                super().merge_one(porudzbina)

            if entity['dostavljanje'] == 'restoran':
                print('usao u dostavljanje ')
                print(entity)
                temp_sto = super().get_entity_for_id(Sto, entity['sto'])
                porudzbina.sto = temp_sto
                super().merge_one(porudzbina)

            email_utils.send_notification_email("bogdan@pico.rs","")
            return "Uspesno dodat entitet", 201
        except:
            return 'Doslo je do greske prilikom unosa', 403


@porudzbina_ns.route('/query/')
@porudzbina_ns.route('/query/<id>')
class PorudzbinaQuery(AbstractService):


    def get_all_aktivne(self):
        status_otvorene = super().query_data(StatusPorudzbine, {"status": "otvorena"})[0]
        status_zatvorena = super().query_data(StatusPorudzbine, {"status": "zatvorena"})[0]
        status_priprema = super().query_data(StatusPorudzbine, {"status": "priprema"})[0]
        result = []
        for r in super().query_data(Porudzbina, {'status_porudzbine_id':status_otvorene.id}):
            result.append(r)
        for r in super().query_data(Porudzbina, {'status_porudzbine_id':status_zatvorena.id}):
            result.append(r)
        for r in super().query_data(Porudzbina, {'status_porudzbine_id':status_priprema.id}):
            result.append(r)
        return sorted(result, key=attrgetter('datum_otvaranja'))

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id):

        result = []
        if id == "aktivne":
            for r in self.get_all_aktivne():
                entity = super().deep_json_from_attributes(r)
                entity["stavke"] = []
                for stavka in eval(entity["istorija_porudzbine"]):
                    entity["stavke"].append(
                        super().deep_json_from_attributes(super().get_entity_for_id(IstorijaPorudzbine, int(stavka))))
                result.append(entity)
            return result
        else:
            return "Greska",404

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):

        result = []

        zahtev = request.get_json(force=True)
        if "aktivne" in zahtev:
            for r in self.get_all_aktivne():
                result.append(super().deep_json_from_attributes(r))
            return result
        else:
            for r in super().query_data(Porudzbina, request.get_json(force=True)):
                result.append(super().deep_json_from_attributes(r))

            return result


@porudzbina_ns.route('/print/')
class PorudzbinaPrint(AbstractService):


    def post(self):

        file_dir = os.path.dirname(os.path.abspath(__file__))
        parent = os.path.dirname(file_dir)
        utils_folder = join(parent, 'win_addons')
        config = pdfkit.configuration(wkhtmltopdf=join(utils_folder, 'wkhtmltopdf.exe'))
        rendered = render_template('porudzbina_pdf.html', data=request.get_json(force=True))
        pdf = pdfkit.from_string(rendered, False, configuration=config)

        response = make_response(pdf)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'attachment; filename = output.pdf'

        return response
