from model.kategorija_entity import Kategorija
from model.podkategorija_entity import PodKategorija
from service.abstract_service import AbstractService
from service.api import podkategorija_ns
from flask import request
from operator import attrgetter
from utils.login_utils import secured


@podkategorija_ns.route('/<int:id>')
@podkategorija_ns.route('/')
class PodkategorijaService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        result = []

        if id is None:

            for r in sorted(super().get_all_entities(PodKategorija), key=attrgetter('naziv')):
                result.append(super().deep_json_from_attributes(r))
            return result

        elif isinstance(id, int):
            return super().deep_json_from_attributes(super().get_entity_for_id(PodKategorija, id))

    @secured(roles=['menadzer', 'administrator'])
    def post(self):
        entity = request.get_json(force=True)

        temp_kategorija = super().get_entity_for_id(Kategorija, entity["kategorija"])

        temp_podkategorija = PodKategorija(entity["naziv"], temp_kategorija)


        super().add_one(temp_podkategorija)

        return "Uspesno dodat", 201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        entity = request.get_json(force=True)

        temp_podkategorija = super().get_entity_for_id(PodKategorija, id)

        if isinstance(entity["kategorija"], int):
            temp_kategorija = super().get_entity_for_id(Kategorija, entity["kategorija"])
            temp_podkategorija.kategorija = temp_kategorija
        else:
            temp_kategorija = super().get_entity_for_id(Kategorija, entity["kategorija"]["id"])
            temp_podkategorija.kategorija = temp_kategorija

        temp_podkategorija.naziv = entity["naziv"]

        super().merge_one(temp_podkategorija)

        return "Uspesno izmenjen entitet", 201

    @secured(roles=['menadzer', 'administrator'])
    def delete(self,id):

        super().delete_one(PodKategorija,id)


@podkategorija_ns.route('/query/')
class PodkategorijaQuery(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):

        result = []
        for r in super().query_data(PodKategorija, request.get_json(force=True)):
            result.append(super().deep_json_from_attributes(r))

        return result