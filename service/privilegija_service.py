from model.privilegija_entity import Privilegija
from model.radnik_entity import Radnik
from service.abstract_service import AbstractService
from service.api import privilegija_ns
from flask import request
from utils.login_utils import secured

@privilegija_ns.route('/')
@privilegija_ns.route('/<int:id>')
class PrivilegijeService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self,id=None):
        if id is None:
            all = []
            for one in super().get_all_entities(Privilegija):
                one = one.asdict()
                all.append(one)
            return all

        elif isinstance(id,int):
            return super().get_entity_for_id(Privilegija, id).asdict()


    def put(self,id):

        return "Uspesno izmenjen entitet", 201

    @secured(roles=['administrator'])
    def delete(self,id):
        super().delete_one(Privilegija,id)

    @secured(roles=['menadzer', 'administrator'])
    def post(self):

        entity = request.get_json(force=True)
        temp_radnik = super().get_entity_for_id(Radnik, entity["radnik"]["id"])
        temp_privilegija = Privilegija(entity["korisnik"], entity["administrator"], entity["menadzer"], temp_radnik)
        super().add_one(temp_privilegija)

        return "Uspesno dodat", 201

@privilegija_ns.route('/query/')
class PrivilegijekQuery(AbstractService):
    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def post(self):
        result = []
        for r in super().query_data(Privilegija, request.get_json(force=True)):
            #privilegije = r.privilegije[0].asdict()
            r = r.asdict()
            #r["privilegije"] = privilegije
            result.append(r)
        return result