from flask_restplus import Resource
from sqlalchemy import create_engine
from sqlalchemy.orm import attributes
from datetime import datetime
from model.stavka_entity import Stavka
from model.radnik_entity import Radnik
from model.grad_entity import Grad
from model.korisnik_entity import Korisnik
from model.status_entity import StatusPorudzbine
from model.kategorija_entity import Kategorija
from model.podkategorija_entity import PodKategorija
from sqlalchemy.orm import sessionmaker
import inspect

engine = create_engine('mysql+mysqlconnector://root:root@localhost/restaurantdb')

Session = sessionmaker(bind=engine)

class AbstractService(Resource):

    def custom_sql(self,sql):
        self.session = Session()
        result = self.session.execute(sql)
        d, a = {}, []
        for rowproxy in result:
            for column, value in rowproxy.items():
                d = {**d, **{column: value}}
            a.append(d)
        self.session.close()
        return a

    def get_all_entities(self, entity):
        self.session = Session()
        result = self.session.query(entity).all()
        self.session.close()
        return result

    def get_entity_for_id(self, entity, id):
        self.session = Session()
        result = self.session.query(entity).filter(entity.id == id).one()
        self.session.close()
        return result

    def get_full_one_json(self, entity,id):
        self.session = Session()
        result = self.session.query(entity).filter(entity.id == id).one()
        self.session.close()
        return result.asdict()

    def get_all_full_json(self,entity):
        self.session = Session()
        all = []
        for one in self.session.query(entity).all():
            all.append(one.asdict())
        self.session.close()
        return all

    def add_one(self,entity):
        self.session = Session()
        if entity.id is not None:
            self.session.merge(entity)
        else:
            self.session.add(entity)
        self.session.commit()
        self.session.close()
        return entity

    def merge_one(self,entity):
        self.session = Session()
        self.session.merge(entity)
        self.session.commit()
        self.session.close()
        return entity

    def delete_one(self,entity,id):
        e = self.get_entity_for_id(entity, id)
        self.session.delete(e)
        self.session.commit()
        self.session.close()
        return True

    def query_data(self, entity, data):
        self.session = Session()
        q = self.session.query(entity)
        for k, v in data.items():
            try:
                k, operator = k.split("__")
            except:
                pass
            f = getattr(entity, k)

            if str(f.property.columns[0].type) == "VARCHAR":
                q = q.filter(f.like('%' + v + '%'))
            elif str(f.property.columns[0].type) == "INTEGER" or str(f.property.columns[0].type) == "BOOLEAN":
                q = q.filter(f == v)
            elif str(f.property.columns[0].type) == "DATETIME":

                if operator == "to":
                    q = q.filter(f <= v + " 23:59:59")
                elif operator == "from":
                    q = q.filter(f >= v + " 00:00:00")
                elif operator == "now":
                    print("now")
                    q = q.filter(f == v)

        self.session.close()
        return q.all()

    def query_meta_data(self,data):
        return

    def json_from_attributes(self, r):
        entity = {}
        for k, v in vars(r).items():
            if not isinstance(v,int):
                v = str(v)
            if not k == "_sa_instance_state":
                if isinstance(v, datetime):
                    v = v.isoformat()
                elif v == "None":
                    v = None
                entity[str(k)] = v
        return entity

    def deep_json_from_attributes(self, r):
        entity = {}
        for k, v in vars(r).items():
            if not isinstance(v,int):
                v = str(v)
            if not k == "_sa_instance_state":
                if isinstance(v, datetime):
                    v = v.isoformat()
                elif v == "None":
                    v = None
                entity[str(k)] = v

        if "podkategorija_id" in entity and entity["podkategorija_id"] is not None:
            entity["podkategorija"] = self.get_full_one_json(PodKategorija, int(entity["podkategorija_id"]))

        if "kategorija_id" in entity and entity["kategorija_id"] is not None:
                entity["kategorija"] = self.get_full_one_json(Kategorija, int(entity["kategorija_id"]))

        try:
            if "kategorija_id" in entity["podkategorija"] and entity["podkategorija"]["kategorija_id"] is not None:
                entity["podkategorija"]["kategorija"] = self.get_full_one_json(Kategorija, int(entity["podkategorija"]["kategorija_id"]))
        except:
            pass


        if "stavka_id" in entity and entity["stavka_id"] is not None:
            entity["stavka"] = self.get_full_one_json(Stavka, int(entity["stavka_id"]))

        if "grad_id" in entity and entity["grad_id"] is not None:
            entity["grad"] = self.get_full_one_json(Grad, int(entity["grad_id"]))

        if "radnik_id" in entity and entity["radnik_id"] is not None:
            entity["radnik"] = self.get_full_one_json(Radnik, int(entity["radnik_id"]))

        if "korisnik_id" in entity and entity["korisnik_id"] is not None:
            entity["korisnik"] = self.get_full_one_json(Korisnik, int(entity["korisnik_id"]))

        if "status_porudzbine_id" in entity and entity["status_porudzbine_id"] is not None:
            entity["status_porudzbine"] = self.get_full_one_json(StatusPorudzbine, int(entity["status_porudzbine_id"]))



        return entity
