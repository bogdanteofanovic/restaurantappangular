from model.kategorija_entity import Kategorija
from service.abstract_service import AbstractService
from service.api import kategorija_ns
from flask import request
from operator import attrgetter
from utils.login_utils import secured


@kategorija_ns.route('/<int:id>')
@kategorija_ns.route('/')
class KategorijaService(AbstractService):
    #@secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self, id=None):
        result = []

        if id is None:

            for r in sorted(super().get_all_entities(Kategorija), key=attrgetter('id')):
                result.append(super().deep_json_from_attributes(r))
            return result

        elif isinstance(id, int):
            return super().deep_json_from_attributes(super().get_entity_for_id(Kategorija, id))

    @secured(roles=['menadzer', 'administrator'])
    def post(self):

        entity = request.get_json(force=True)
        temp_kategorija = Kategorija(entity["naziv"])
        super().add_one(temp_kategorija)

        return "Uspesno dodat", 201

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        temp_kategorija = super().get_entity_for_id(Kategorija, id)
        entity = request.get_json(force=True)
        temp_kategorija.naziv = entity["naziv"]
        temp_kategorija.aktivna = entity["aktivna"]
        super().merge_one(temp_kategorija)

        return "Uspesno izmenjen entitet", 201

    @secured(roles=['administrator'])
    def delete(self,id):

        super().delete_one(Kategorija,id)

        return "Uspesno obrisana kategorija", 201
