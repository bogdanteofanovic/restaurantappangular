from model.porudzbina_entity import Porudzbina
from model.status_entity import StatusPorudzbine
from model.radnik_entity import Radnik
from model.korisnik_entity import Korisnik
from service.abstract_service import AbstractService
from service.api import status_ns
from flask import request
from datetime import datetime
from operator import itemgetter
from utils.login_utils import secured

@status_ns.route('/')
@status_ns.route('/<int:id>')
class StatusService(AbstractService):

    @secured(roles=['korisnik', 'menadzer', 'administrator'])
    def get(self,id=None):
        if id is None:

            all = []
            for one in super().get_all_entities(StatusPorudzbine):
                all.append(one.asdict())

            return all

        elif isinstance(id,int):
            one = super().get_entity_for_id(StatusPorudzbine, id)

            return one.asdict()

    @secured(roles=['menadzer', 'administrator'])
    def put(self,id):

        edited_status = request.get_json(force=True)
        temp_status = super().get_entity_for_id(StatusPorudzbine,id)
        temp_status.status = edited_status["status"]
        super().merge_one(temp_status)

        return "Uspesno izmenjen status", 201

    @secured(roles=['administrator'])
    def delete(self,id):

        super().delete_one(StatusPorudzbine,id)

        return "Uspesno obrisan status", 201

    @secured(roles=['menadzer', 'administrator'])
    def post(self):

        entity = request.get_json(force=True)
        temp_status = StatusPorudzbine(entity["status"])
        super().add_one(temp_status)

        return "Uspesno dodat", 201


@status_ns.route('/query/')
class StatusQuery(AbstractService):

    def post(self):

        result = []

        return result
